﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CoinsManager : MonoBehaviour
{
	public static int Coins;        

	public Text text;                      

	void Awake ()
	{
		text = GetComponent <Text> ();
		Coins = 0;
	}

	void Update ()
	{
		text.text = "Coins: " + Coins;
	}
}