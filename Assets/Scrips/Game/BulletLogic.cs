﻿using UnityEngine;
using System.Collections;


public class BulletLogic : MonoBehaviour
{
	public void DoTrajectory(Vector3 target)
	{

		iTween.MoveTo (gameObject, target, 100f); 
		float angle = Mathf.Atan2 (target.y, target.x) * Mathf.Rad2Deg;
		transform.rotation = Quaternion.AngleAxis (angle, Vector3.forward);
		Invoke ("DestroyBullet", 4.0f);
	}

	private void DestroyBullet() 
	{
		Destroy (gameObject);
	}

	void OnCollisioEnter2D (Collision2D col)
	{
		if (col.gameObject.tag == "Zombie")
			Destroy (this.gameObject);
	}
}
