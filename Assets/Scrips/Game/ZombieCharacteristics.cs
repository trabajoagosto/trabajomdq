﻿using UnityEngine;
using System.Collections; 
using System;


public class ZombieCharacteristics : MonoBehaviour 
{
	
	public float Speed;
	public int Live;
	public Rigidbody2D Myrigibody;
	public int CoinsValue = 5;
	public int CoinsValues = 7;


	private bool ModeZombie;
	private bool ModeNoZombie;




	void Start ()
	{
		Myrigibody = GetComponent <Rigidbody2D> ();
		ModeZombie = true;
		ModeNoZombie = false;


	}

	void Update ()
	{
		
		if (ModeZombie == true)
			this.gameObject.transform.Translate (Vector2.left * Speed * Time.deltaTime);
		
		if (ModeNoZombie == true)
			this.gameObject.transform.Translate (Vector2.right * Speed * Time.deltaTime);
		
		if (Live == 0)
		{
			CoinsManager.Coins += CoinsValue; 
			Destroy (this.gameObject);
			 
		}
	}
	void OnTriggerStay2D (Collider2D Stay)
	{
		if (Stay.gameObject.CompareTag ("Boom"))
			Live -= Live;
		   CoinsManager.Coins += CoinsValues; 
	}
	void OnCollisionEnter2D (Collision2D col)
	{
		/*if (col.gameObject.CompareTag ("Vaccine"))
		{
		    ModeZombie = false;
			ModeNoZombie = true;
			this.gameObject.tag = "NoZombie";
			;

		}*/
		if (ModeZombie == true) 
		{
			if (col.gameObject.tag == "Bullet") {
				Destroy (col.gameObject);
				Myrigibody.AddForce (Vector2.left * 4, ForceMode2D.Impulse);
				Live--;
			}
			if (col.gameObject.tag == "NoZombie")
			{
				Live--;
				Myrigibody.AddForce (Vector2.left * 10, ForceMode2D.Impulse);
			}
		}
		if (ModeNoZombie == true) 
		{			
			
			if (col.gameObject.tag == "Bullet")
				Destroy (col.gameObject);

			if (col.gameObject.tag == "Zombie")
			{
				Myrigibody.AddForce( Vector2.right * 10, ForceMode2D.Impulse);
				Live--;
			}
		
		
		}
	
	}

}




