﻿using UnityEngine;

using System.Collections;

public class GameObjectSpeed : MonoBehaviour {
	// Publics Variables
	public float Speed;
	public Vector2 Direction;
	public PolygonCollider2D PolCol;
	public Rigidbody2D MyRigibody2D;
	public float TimeOfDestroid;
	public float TimeBoom;
	public float TimeChangeColor;

	//Privates vatiables
	private bool Boom;
	public bool Vaccine;
	public bool Grenade;

	void Start()
	{
		MyRigibody2D = GetComponent <Rigidbody2D> ();
		PolCol = GetComponent <PolygonCollider2D> ();
	}
	void Update()
	{
		transform.Translate (Direction  * Speed * Time.deltaTime);
		if (Grenade == true) {
			Invoke ("ChangeColor", TimeChangeColor);
			Invoke ("BoomGrenade", TimeBoom);
		}
		if (Boom == true) 
		{
			this.gameObject.tag = "Boom" ;
			Destroy (this.gameObject, TimeOfDestroid);
		}
	}

	void BoomGrenade()
	{
		Boom = true;
	}

	void ChangeColor()		
	{
		gameObject.GetComponent<Renderer>().material.color = Color.red;
	}

	void OnCollisionEnter2D (Collision2D col)
	{
		/*
		if (Vaccine == true)
		{
			if (col.gameObject.tag == this.gameObject.tag)
				return;
			else 
			{
				Speed = 0;
				PolCol.enabled = false;
				MyRigibody2D.isKinematic = true;
				Destroy (this.gameObject, TimeOfDestroid);
			}
		}*/
		if (Grenade == true)
		{
			if (col.gameObject.tag == this.gameObject.tag)
				return;
			else 
			{
				Speed = 0;
			}
		}
	}
}





