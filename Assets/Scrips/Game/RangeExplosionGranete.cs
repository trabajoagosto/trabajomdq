﻿using UnityEngine;
using System.Collections;

public class RangeExplosionGranete : MonoBehaviour
{
	private bool Boom;
	public float TimeBoom;
	public float TimeChangeColor;


	GameObject SpawnEnemy;
	ZombieCharacteristics LiveZombie;
	int Livezombie;
	void Start () 
	{
		Livezombie = GetComponent<ZombieCharacteristics> ().Live;
		Boom = false;
	}
	
	void Update () 
	{
		Invoke ("ChangeColor", TimeChangeColor);
		Invoke ("BoomGrenade", TimeBoom);
		if (Boom == true) {
			this.gameObject.tag = "Boom" ;
			Destroy (this.gameObject);
		}
	}
	void BoomGrenade()
	{
		Boom = true;
	}
	void ChangeColor()
	{
		gameObject.GetComponent<Renderer>().material.color = Color.red;
	}

	void OnTriggerEnter2D (Collider2D Enter)
	{
		if (Boom == true) 
		{
			if (Enter.gameObject.CompareTag ("Zombie"))
			{
				//Stay.gameObject.GetComponent<ZombieCharacteristics> ().Live -= GetComponent<ZombieCharacteristics> ().Live;
			}
		}
	}
}
