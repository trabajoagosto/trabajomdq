﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class TorretControl : MonoBehaviour {
	public GameObject BulletPrefab;
	public GameObject[] SpawnPoint;
	public float TimeBetWeenBullets;

	private bool _turretEnabled;
	AudioSource AudioDisparo;
	bool Audio;


	void Awake()
	{
		AudioDisparo = GetComponent <AudioSource> ();
		Audio = false;

	}
	void Start () 
	{
		_turretEnabled = true;


	}
	

	void Update ()
	{
	  if (Input.GetMouseButtonUp (0)) 
		{
			
			if ( _turretEnabled) 
				Invoke ("SpawnBullet", 0.0f);
				Audio = true;		
										
		} 
		if (Audio) 
		{
			AudioDisparo.Play ();
			Audio = false;
		}	
	
	}
	public void SpawnBullet()
	{
		var randomSpawnIndex = Random.Range (0, SpawnPoint.Length); 
		GameObject go = (GameObject)Instantiate (BulletPrefab, SpawnPoint [randomSpawnIndex].transform.position, Quaternion.identity);
		go.transform.position = SpawnPoint [randomSpawnIndex].transform.position;
		go.SetActive (true);
		go.GetComponent<BulletLogic> ().DoTrajectory (Input.mousePosition - Camera.main.WorldToScreenPoint (go.transform.position));


		_turretEnabled = false;
		Invoke ("ReenableTurret", 0.0f); 	
		 
	}

	private void ReenableTurret() 
	{
		_turretEnabled = true;

	}



}
