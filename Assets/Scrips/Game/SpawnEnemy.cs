﻿using UnityEngine;
using System.Collections;

public class SpawnEnemy : MonoBehaviour {

	public GameObject Enemy;
	float maxSpawnRateInSeconds = 5f;


	void Start ()
	{
		Invoke ("SpawnsEnemys", maxSpawnRateInSeconds);
		InvokeRepeating ("IncreaseSpawnRate", 0f, 30f);
	}
	

	void Update () 
	{

	}

	void SpawnsEnemys()
	{
		Vector2 min = Camera.main.ViewportToWorldPoint (new Vector2 (0, 0));
		Vector2 max = Camera.main.ViewportToWorldPoint (new Vector2 (1, 1));

		GameObject anEnemy = (GameObject)Instantiate (Enemy);
		anEnemy.transform.position = new Vector2 (Random.Range (min.x, max.x), max.y);

		SiguienteSpawnEnemy ();
	}

	void SiguienteSpawnEnemy()
	{
		float spawnInSeconds;
		if (maxSpawnRateInSeconds > 1f) {
			spawnInSeconds = Random.Range (1f, maxSpawnRateInSeconds);
		} else
			spawnInSeconds = 1f;

		Invoke ("SpawnsEnemys", spawnInSeconds);
	}

	void IncreaseSpawnRate ()
	{
		if (maxSpawnRateInSeconds > 1f) 
		{
			maxSpawnRateInSeconds--;
		}
		if (maxSpawnRateInSeconds == 1f)
			CancelInvoke ("increaseSpawnRate");
	}
}
