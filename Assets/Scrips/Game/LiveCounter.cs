﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LiveCounter : MonoBehaviour {

	public int startingHealth = 100;
	public float currentHealth;
	public float EnemyDamage;
	bool Player;
	public Slider healthSlider;

	//GameObject SpawnEnemy;
	//EnemyInvoked EnemyInvoked;

	void Awake ()
	{
		Player = false;
		//SpawnEnemy = GameObject.FindGameObjectWithTag ("Zombie");
		//EnemyInvoked = SpawnEnemy.GetComponent<EnemyInvoked> ();
		currentHealth = startingHealth;

	}
	void Update ()
	{
		if (currentHealth <= 0)
		{
			if (Player == true) {
				SceneManager.LoadScene (0); 

			}

		}

	}
	void OnCollisionStay2D (Collision2D col)
	{
		if (col.gameObject.CompareTag ("Zombie")) 
		{
			currentHealth -= EnemyDamage * Time.deltaTime;
			healthSlider.value = currentHealth;
			if (currentHealth <= 0) 
			{
				Death ();
			}
		}
	}

	void Death()
	{
		//EnemyDamage = 0;
		//Time.timeScale = 0;
		SceneManager.LoadScene ("GameOver");
		//EnemyInvoked = 
	}

	/*
	void OnTriggerStay2D (Collider2D col)
	{
		if (Torret == true )
		{
			if (Live <= 0)
			{
				if (col.gameObject.CompareTag ("Zombie"))
				{
					Destroy (col.gameObject);

				}
			}
		}
	}*/
}
