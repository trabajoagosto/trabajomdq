﻿using UnityEngine;
using System.Collections;

public class InvokeButton : MonoBehaviour 
{
	public GameObject Object;
	public AudioSource Audio;

	public void Actionate ()
	{
		Instantiate (Object, transform.position, transform.rotation);
		Audio.Play ();
	}

}
