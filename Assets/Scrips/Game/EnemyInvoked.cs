﻿using UnityEngine;
using System.Collections;

public class EnemyInvoked: MonoBehaviour { 
	public GameObject Zombie;
	public GameObject[] SpawnPoint;
	public int Maxspawn;


	public float TimeBetWeenEnemies = 0.1f;



	// Use this for initialization
	void Start () 
	{ 
		
		if (Maxspawn <= 5) 
		{
			Invoke (); 
		} else 
		{
			CancelInvoke ();
		}
	
	}
	void Invoke ()
	{
		InvokeRepeating ("SpawnEnemy", 0.5f, TimeBetWeenEnemies);
	}

	public void SpawnEnemy()
	{
		var randomSpawnIndex = Random.Range (0, SpawnPoint.Length); 
		var go = Instantiate (Zombie, SpawnPoint[randomSpawnIndex].transform.position, Quaternion.identity);


	}

	void OnCollisionEnter2D (Collision2D col)
	{
		if (col.gameObject.tag == "Bullet")
			Destroy (this.gameObject);
	}

}