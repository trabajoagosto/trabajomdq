﻿using UnityEngine;
using System.Collections;

public class MissileSpeed : MonoBehaviour {

	public float Speed;
	public int CoinsValues = 10;

	void Update()
	{
		transform.Translate (Vector2.left * Speed * Time.deltaTime);
		Invoke ("DestroidMisile", 10);
	}
	 
	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject)
		{
			Destroy (other.gameObject);
			CoinsManager.Coins += CoinsValues; 
		}

	}

	void DestroidMisile ()
	{
		Destroy (this.gameObject);
	}
}
